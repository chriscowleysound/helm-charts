build:
	rm -f public/*
	curl  "https://${REPO_URL}/index.yaml" > public/index.yaml
	helm package ./charts/* --destination ./public
	helm repo index --url "https://${REPO_URL}/" --merge public/index.yaml ./public
	cat artifacthub-repo.yml.in | envsubst > public/artifacthub-repo.yml

upload:
	gsutil rsync public/ "gs://${REPO_URL}/"
