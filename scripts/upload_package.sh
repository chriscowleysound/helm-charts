#!/bin/sh

FILE=$1
PACKAGE=$(basename ${FILE} | cut -d '-' -f 1)
VERSION=$(echo $(basename ${FILE} | cut -d '-' -f 2 | sed 's/.tgz//'))

curl -X PUT --header "JOB-TOKEN: $CI_JOB_TOKEN" \
  --upload-file ${FILE} \
  "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${PACKAGE}/${VERSION}/$(basename ${FILE})"
